<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_assignment');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Jb[G14o$;`xO9F@Z>nVCVUVfh`6#}##z$lq .+5EJ-7d&u<GdjJ41pf.e|eL vYv');
define('SECURE_AUTH_KEY',  '{eyRB`*tY1v[4fFrj#3~L(alVF.1/A2$2vI4(DnEAxfh^y-,v|`u$G}2{0J&Jv(M');
define('LOGGED_IN_KEY',    'n/ociuW2lllg]Q9!W]TIDjz;^{ 2Vu0y7C#|gLqIUP54[<wZEDyA7tC=4SQJHRXd');
define('NONCE_KEY',        '[w 5Dvo$K#Xr_+>W;R`!,r7Xef:@MaS=cO%GuG]N(Ysg-}s+kml~c+vb-p{i]pW+');
define('AUTH_SALT',        '#0W+Z{t:p@ew1-^q4r1{pdDUU;.P$W=/fb/vdTXk%Tc-_]4^dche{)^b4zDPni96');
define('SECURE_AUTH_SALT', ',h;9_$5 8y0Rbbm&n>pVeo,%Svvj0`luzFN5;wR3-mf|@4nhw1#WK/l|_KdwL$Hi');
define('LOGGED_IN_SALT',   '`Id1QCN3oa~j.a0h+-fe&vo)%(=:}!x8tc@Mq-C?i|ELk)A$^ChQNMtw4fY3HB i');
define('NONCE_SALT',       'K(2!!`]4#1%^kM@Dsd<Kt!Us!dL,+l*T:C@]pd5iHh+=m>2RC?mDao<0zarDT[?Z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
