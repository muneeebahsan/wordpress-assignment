<?php

/*
* Plugin Name: Custom Post Type
* Description: My first custom post type plugin.
* Author: Muneeb
* Version: 1.0
*/

// Creat Student Custom Post Type
function cpt_create_student_custom_post() {

	$labels = array(
		'name' => __( 'Students' ),
		'singular_name' => __( 'Student'),
		'menu_name' => __( 'Students'),
		'parent_item_colon' => __( 'Parent Student'),
		'all_items' => __( 'All Students'),
		'view_item' => __( 'View Student'),
		'add_new_item' => __( 'Add New Student'),
		'add_new' => __( 'Add New'),
		'edit_item' => __( 'Edit Student'),
		'update_item' => __( 'Update Student'),
		'search_items' => __( 'Search Student'),
		'not_found' => __( 'Not Found'),
		'not_found_in_trash' => __( 'Trash is empty')
	);

	$args = array(
		'label' => __( 'students'),
		'labels' => $labels,
		'supports' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'comments' ),
		'taxonomies' => array( 'courses' ),
		'public' => true,
		'hierarchical' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-admin-users',
		'show_in_nav_menus' => true,
		'show_in_admin_bar' => true,
		'has_archive' => true,
		'can_export' => true,
		'capability_type' => 'post'
);

	// Register Custom Post
	register_post_type( 'students', $args );
}

// Add hook 
add_action( 'init', 'cpt_create_student_custom_post' );
 
// Create Course Custom Taxonomy 
function cpt_create_course_custom_taxonomy() {
 
  $labels = array(
    'name' => _x( 'Courses', 'taxonomy general name' ),
    'singular_name' => _x( 'Course', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Courses' ),
    'all_items' => __( 'All Courses' ),
    'parent_item' => __( 'Parent Course' ),
    'parent_item_colon' => __( 'Parent Course:' ),
    'edit_item' => __( 'Edit Course' ), 
    'update_item' => __( 'Update Course' ),
    'add_new_item' => __( 'Add New Course' ),
    'new_item_name' => __( 'New Course Name' ),
    'menu_name' => __( 'Courses' ),
  ); 	
 
  register_taxonomy('courses',array('students'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'course' ),
  ));
}

add_action( 'init', 'cpt_create_course_custom_taxonomy' );

// Create Student Form Metabox
add_action( 'add_meta_boxes', 'cpt_create_student_form_metabox' );

function cpt_create_student_form_metabox() {

    add_meta_box( 'student_form', 'Student Form', 'student_form_callback', 'students' );

}

function student_form_callback( $post ) {

    wp_nonce_field( 'save_student_info', 'save_student_info_nonce' );

    $cpt_studentName = get_post_meta( $post->ID, '_studentName_key', true );
    $cpt_fatherName = get_post_meta( $post->ID, '_fatherName_key', true );
    $cpt_dob = get_post_meta( $post->ID, '_dob_key', true );
    $cpt_gender = get_post_meta( $post->ID, '_gender_key', true );
    $cpt_mobileNo = get_post_meta( $post->ID, '_mobileNo_key', true );
    $cpt_email = get_post_meta( $post->ID, '_email_key', true );
    $cpt_address = get_post_meta( $post->ID, '_address_key', true );
    $cpt_studyProgram = get_post_meta( $post->ID, '_studyProgram_key', true );
    $cpt_hobby = get_post_meta( $post->ID, '_hobby_key', true );

    function cpt_html($str){

        return htmlspecialchars($str, ENT_QUOTES);
    }


 ?>
 <!DOCTYPE html>
 <html>
 <head>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
 </head>
 <body>

    <table class="table table-bordered table-light">
 

                <tr>
                    <td><label for="cpt_studentName">Student Name: </label></td>
                    <td><input type="text" class="form-control" name="cpt_studentName" id="cpt_studentName" value="<?php echo cpt_html($cpt_studentName); ?>"></td>
                </tr>

                <tr>
                    <td>Father Name:</td>
                    <td><input type="text" class="form-control" name="cpt_fatherName" id="cpt_fatherName" value="<?php echo cpt_html($cpt_fatherName); ?>"></td>
                </tr>

                <tr>
                    <td>Date of Birth:</td>
                    <td><input type="date" class="form-control" name="cpt_dob" id="cpt_dob" value="<?php echo cpt_html($cpt_dob); ?>"></td>
                </tr>

                <tr>
                    <td>Gender:</td>
                    <td>
                        <input type="radio" class="form-control" name="cpt_gender" id="cpt_gender" value="Male" <?php if ($cpt_gender == 'Male') echo 'checked'; ?>>
                         Male <br>
                        <input type="radio" name="cpt_gender" id="cpt_gender" value="Female" <?php if ($cpt_gender == 'Female') echo 'checked'; ?>> Female
                    </td>
                </tr>

                <tr>
                    <td>Mobile No:</td>
                    <td><input type="text" class="form-control" name="cpt_mobileNo" id="cpt_mobileNo" value="<?php echo cpt_html($cpt_mobileNo); ?>" maxlength="11"></td>
                </tr>

                <tr>
                    <td>Email:</td>
                    <td><input type="email" class="form-control" name="cpt_email" id="cpt_email" value="<?php echo cpt_html($cpt_email); ?>"></td>
                </tr>

                <tr>
                    <td>Address:</td>
                    <td><textarea class="form-control" name="cpt_address" id="cpt_address" rows="3"><?php echo cpt_html($cpt_address); ?></textarea></td>
                </tr>

                <tr>
                    <td>Study Program:</td>
                    <td>
                       
                        <select class="form-control" name="cpt_studyProgram">
                            <option>Select Study Program</option>

                        <?php
                        $cpt_array = array("BSC (2 Years)", "BS (2 Years)", "BS (4 Years)", "MS (2 Years)", "MSC (2 Years)", "M-Phil", "PHD");  

                        foreach ($cpt_array as $value) {

                        ?>
                            <option  value="<?php echo $value; ?>"
                                <?php echo $cpt_studyProgram ==  $value ? 'selected' : ''; ?>>
                                <?php echo $value; ?>
                            </option>
                         
                        <?php
                                                        
                        }

                        ?>

                        </select>
                    </td>
                </tr>

                <tr>
                    <td>Hobby:</td>
                    <td>
                        <?php $cpt_hobbies = explode(",", $cpt_hobby); ?>
                        <input class="form-control" type="checkbox" name="cpt_hobby[]" value="Reading"<?php  if (in_array('Reading', $cpt_hobbies)){ echo "checked='checked'"; }?>>Reading
                        <input class="form-control" type="checkbox" name="cpt_hobby[]" value="Sports"<?php  if (in_array('Sports', $cpt_hobbies)){ echo "checked='checked'"; }?>>Sports
                        <input class="form-control" type="checkbox" name="cpt_hobby[]" value="Traveling"<?php  if (in_array('Traveling', $cpt_hobbies)){ echo "checked='checked'"; }?>>Traveling
                        <input class="form-control" type="checkbox" name="cpt_hobby[]" value="None"<?php  if (in_array('None', $cpt_hobbies)){ echo "checked='checked'"; }?>>None
                    </td>
                </tr>


            </table>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 
 </body>
 </html>
    

<?php
}

// Sava Meta Box Value
add_action( 'save_post', 'cpt_save_student_form' );

function cpt_save_student_form( $post_id ) {

     if ( !isset( $_POST['save_student_info_nonce'] ) ) {
        return $post_id;
    }

    if ( !wp_verify_nonce( $_POST['save_student_info_nonce'], 'save_student_info' ) ) {
        return $post_id;
    }

    if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
        return $post_id;
    }

    if ( !current_user_can( 'edit_post' ) ) {
        return $post_id;
    }

    // Save Meta Box Data in Database
    $cpt_student_name = sanitize_text_field( $_POST['cpt_studentName'] );
    update_post_meta( $post_id, '_studentName_key', $cpt_student_name );

    $cpt_father_name = sanitize_text_field( $_POST['cpt_fatherName'] );
    update_post_meta( $post_id, '_fatherName_key', $cpt_father_name );

    $cpt_date_of_birth = esc_attr( $_POST['cpt_dob'] );
    update_post_meta( $post_id, '_dob_key', $cpt_date_of_birth );

    $cpt_gend = esc_attr( $_POST['cpt_gender'] );
    update_post_meta( $post_id, '_gender_key', $cpt_gend );

    $cpt_mobile_no = sanitize_text_field( $_POST['cpt_mobileNo'] );
    update_post_meta( $post_id, '_mobileNo_key', $cpt_mobile_no );

    $cpt_mail = sanitize_text_field( $_POST['cpt_email'] );
    update_post_meta( $post_id, '_email_key', $cpt_mail );

    $cpt_addr = sanitize_textarea_field( $_POST['cpt_address'] );
    update_post_meta( $post_id, '_address_key', $cpt_addr );

    $cpt_study_program = esc_attr( $_POST['cpt_studyProgram'] );
    update_post_meta( $post_id, '_studyProgram_key', $cpt_study_program );

    $cpt_hobb = implode( ",", $_POST['cpt_hobby'] );
    update_post_meta( $post_id, '_hobby_key', $cpt_hobb );


}


