<?php

if (have_posts()) :

	while (have_posts()) : the_post(); ?>

		<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

		<?php 

		the_content();

		echo 'Student Name: ';
		$cpt_studentName = get_post_meta( $post->ID, '_studentName_key', true );
		echo $cpt_studentName; ?> <br>

		<?php
		echo 'Father Name: ';
		$cpt_fatherName = get_post_meta( $post->ID, '_fatherName_key', true );
		echo $cpt_fatherName; ?> <br>

   	 	<?php
   	 	echo 'Date of Birth: ';
   	 	$cpt_dob = get_post_meta( $post->ID, '_dob_key', true );
   	 	echo $cpt_dob; ?> <br>

    	<?php 
    	echo 'Gender: ';
    	$cpt_gender = get_post_meta( $post->ID, '_gender_key', true );
    	echo $cpt_gender; ?> <br>

    	<?php
    	echo 'Mobile No: ';
    	$cpt_mobileNo = get_post_meta( $post->ID, '_mobileNo_key', true );
    	echo $cpt_mobileNo; ?> <br>

    	<?php
    	echo 'Email: ';
    	$cpt_email = get_post_meta( $post->ID, '_email_key', true );
    	echo $cpt_email; ?> <br>

    	<?php 
    	echo 'Address: ';
    	$cpt_address = get_post_meta( $post->ID, '_address_key', true );
    	echo $cpt_address; ?> <br>

    	<?php
    	echo 'Study Program: ';
    	$cpt_studyProgram = get_post_meta( $post->ID, '_studyProgram_key', true );
    	echo $cpt_studyProgram; ?> <br>

    	<?php
    	echo 'Hobby: ';
    	$cpt_hobby = get_post_meta( $post->ID, '_hobby_key', true );
    	echo $cpt_hobby;


	endwhile;

else :
	echo '<p>No content found.</p>';

endif;